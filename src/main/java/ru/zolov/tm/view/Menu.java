package ru.zolov.tm.view;

import ru.zolov.tm.service.ProjectService;
import ru.zolov.tm.service.TaskService;

import java.util.Scanner;

public class Menu {
    protected String inputText = "Input: ";
    private Scanner scanner;
    private ProjectService projects;
    private TaskService tasks;


    public Menu(Scanner scanner, ProjectService projects, TaskService tasks) {
        this.scanner = scanner;
        this.projects = projects;
        this.tasks = tasks;
    }


    public void drawHeader() {
        System.out.println("+-------------------------------+");
        System.out.println("|  Welcome to Task manager 0.1  |");
        System.out.println("+-------------------------------+");
        System.out.println("Enter help for display list of commands");
        System.out.print(inputText);
    }

    public void drawHelp() {
        System.out.println("Enter cp for create project");
        System.out.println("Enter dp for display list of project");
        System.out.println("Enter ep for edit project");
        System.out.println("Enter rp for remove project");
        System.out.println("Enter ct for create task");
        System.out.println("Enter dt for display tasks");
        System.out.println("Enter et for edit task");
        System.out.println("Enter ct for remove some task");
        System.out.print(inputText);
    }

    public void createProject() {
        String input;
        System.out.print("Enter project name:");
        input = scanner.nextLine();
        projects.create(input);
        System.out.println("[OK]");
        System.out.print(inputText);
    }

    public void displayProjects() {
        output(projects.readAll());
        System.out.println(inputText);
    }

    public void editProject() {
        System.out.println("Enter id:");
        String inputId = scanner.nextLine();
        System.out.println("Enter project new name");
        String name = scanner.nextLine();
        projects.update(inputId, name);
        System.out.print(inputText);
    }

    public void removeProject() {
        output(projects.readAll());
        System.out.println("Enter id:");
        String input = scanner.nextLine();
        System.out.println(projects.remove(input));

        System.out.print(inputText);
    }

    public void createTask() {
        output(projects.readAll());
        System.out.print("Enter project id: ");
        String projectId = scanner.nextLine();

        tasks.create(projectId);
        tasks.readTaskByProjId(projectId);
        System.out.print(inputText);
    }

    public void removeTask() {
        tasks.readAll();
        System.out.print("Enter task id: ");
        String input = scanner.nextLine();
        tasks.remove(input);
        System.out.println("DONE!");
        System.out.print(inputText);
    }

    public void editTask() {
        output(tasks.readAll());
        System.out.print("Enter task id: ");
        String input = scanner.nextLine();
        System.out.println("Enter new description: ");
        String description = scanner.nextLine();
        tasks.update(input, description);
        System.out.println("DONE!");
        tasks.readTaskById(input);
        System.out.print(inputText);
    }

    public void displayTasks() {
        System.out.print("Enter project id: ");
        String input = scanner.nextLine();
        output(tasks.readTaskByProjId(input));
        System.out.print(inputText);
    }

    public void output(Iterable<?> objects) {
        for (Object object : objects) {
            System.out.println(object);
        }
    }
}
