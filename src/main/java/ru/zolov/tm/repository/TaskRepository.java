package ru.zolov.tm.repository;

import ru.zolov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class TaskRepository {
    private List<Task> taskList = new ArrayList<Task>();

    public void create(String projectId) {
        taskList.add(new Task(projectId));
    }

    public List<Task> readAll() {
        return taskList;
    }

    public List<Task> readTaskByProjId(String id) {
        List<Task> result = new ArrayList<>();
        for (Task t : taskList) {
            if (id.equals(t.getProjectId())) {
                result.add(t);
            }
        }
        return result;
    }

    public Task readTaskById(String id) {

        for (Task t : taskList) {
            if (id.equals(t.getId())) {
                return t;
            }
        }
        return null;
    }

    public boolean update(String id, String description) {
        for (Task t : taskList) {
            if (id.equals(t.getId())) {
                t.setDescription(description);
                return true;
            }
        }
        return false;
    }

    public boolean remove(String id) {
        for (Task t : taskList) {
            if (id.equals(t.getId())) {
                taskList.remove(t);
                return true;
            }
        }
        return false;
    }

    public boolean removeAllByProjectID(String id) {
        boolean isRemoved = false;
        ListIterator<Task> listIterator = taskList.listIterator();
        while (listIterator.hasNext()) {
            if (id.equals(listIterator.next().getProjectId())) {
                listIterator.remove();
                isRemoved = true;
            }
        }
        return false;
    }
}


