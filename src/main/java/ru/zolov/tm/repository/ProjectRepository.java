package ru.zolov.tm.repository;

import ru.zolov.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository {
    private List<Project> projectList = new ArrayList<Project>();

    public ProjectRepository() {
    }

    public void create(String name) {
        projectList.add(new Project(name));
    }

    public Project read(String id) {
        for (Project p : projectList) {
            if (id.equals(p.getId())) {
                return p;
            }
        }
        return null;
    }

    public List<Project> readAll() {
        return projectList;
    }

    public void update(String id, String name) {
        for (Project p : projectList) {
            if (id.equals(p.getId())) {
                p.setName(name);
            }
        }
    }

    public boolean remove(String id) {
        for (Project p : projectList) {
            if (id.equals(p.getId())) {
                projectList.remove(p);
                return true;
            }
        }
        return false;
    }

}
