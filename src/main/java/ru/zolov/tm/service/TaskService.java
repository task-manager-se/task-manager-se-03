package ru.zolov.tm.service;

import ru.zolov.tm.entity.Task;
import ru.zolov.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {
    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }


    public void create(String name) {
        taskRepository.create(name);
    }

    public List<Task> readAll() {
        return taskRepository.readAll();
    }

    public List<Task> readTaskByProjId(String id) {
        return taskRepository.readTaskByProjId(id);
    }

    public Task readTaskById(String id) {
        return taskRepository.readTaskById(id);
    }

    public void remove(String id) {
        taskRepository.remove(id);
    }

    public void update(String id, String description) {
        taskRepository.update(id, description);
    }

}
