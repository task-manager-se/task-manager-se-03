package ru.zolov.tm.service;

import ru.zolov.tm.entity.Project;
import ru.zolov.tm.repository.ProjectRepository;
import ru.zolov.tm.repository.TaskRepository;

import java.util.List;

public class ProjectService {

    private ProjectRepository projectRepository;
    private TaskRepository taskRepository;

    public ProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public void create(String name) {
        projectRepository.create(name);
    }

    public Project read(String id) {
        return projectRepository.read(id);
    }

    public List<Project> readAll() {
        return projectRepository.readAll();
    }

    public boolean update(String id, String name) {
        for (Project p : projectRepository.readAll()) {
            if (id.equals(p.getId())) {
                p.setName(name);
                return true;
            }
        }
        return false;
    }


    public boolean remove(String id) {
        taskRepository.removeAllByProjectID(id);
        return projectRepository.remove(id);
    }
}
