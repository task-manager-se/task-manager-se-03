package ru.zolov.tm;

import ru.zolov.tm.repository.ProjectRepository;
import ru.zolov.tm.repository.TaskRepository;
import ru.zolov.tm.service.ProjectService;
import ru.zolov.tm.service.TaskService;
import ru.zolov.tm.view.Menu;
import ru.zolov.tm.view.MenuItem;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        ProjectRepository projectRepository = new ProjectRepository();
        TaskRepository taskRepository = new TaskRepository();
        ProjectService projects = new ProjectService(projectRepository, taskRepository);
        TaskService tasks = new TaskService(taskRepository);
        Scanner scanner = new Scanner(System.in);
        Menu menu = new Menu(scanner, projects, tasks);
        String input;

        menu.drawHeader();

        do {
            input = scanner.nextLine();
            MenuItem menuItem = MenuItem.checkName(input);
            if (menuItem == null) {
                System.out.println("Wrong input!");
                System.out.println("Input: ");
                continue;
            }

            switch (menuItem) {
                case HELP:
                    menu.drawHelp();
                    break;
                case CREATE_PROJECT:
                    menu.createProject();
                    break;
                case DISPLAY_PROJECTS:
                    menu.displayProjects();
                    break;
                case EDIT_PROJECT:
                    menu.editProject();
                    break;
                case REMOVE_PROJECT:
                    menu.removeProject();
                    break;
                case CREATE_TASK:
                    menu.createTask();
                    break;
                case REMOVE_TASK:
                    menu.removeTask();
                    break;
                case EDIT_TASK:
                    menu.editTask();
                    break;
                case DISPLAY_TASKS:
                    menu.displayTasks();
            }
        } while (!input.equals("exit"));

    }
}

